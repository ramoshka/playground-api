<?php
/**
 * Created 2017-11-10 17:46
 */

declare(strict_types=1);


namespace App\Serializer\Handler;

use JMS\Serializer\Context;
use FOS\RestBundle\Serializer\Normalizer\FormErrorHandler as FOSFormErrorHandler;
use JMS\Serializer\XmlSerializationVisitor;
use Symfony\Component\Form\Form;

/**
 * Class FormErrorHandler
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Serializer\Handler
 */
class FormErrorHandler extends FOSFormErrorHandler
{
    /**
     * @param XmlSerializationVisitor $visitor
     * @param Form $form
     * @param array $type
     * @param Context $context
     * @return mixed
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    public function serializeFormToXml(
        XmlSerializationVisitor $visitor,
        Form $form,
        array $type,
        Context $context = null
    ) {
        if ($context) {
            $statusCode = $context->attributes->get('status_code');
            if ($statusCode->isDefined()) {
                if (null === $visitor->document) {
                    $visitor->document = $visitor->createDocument(null, null, true);
                }

                $codeNode = $visitor->document->createElement('code');
                $visitor->getCurrentNode()->appendChild($codeNode);
                $codeNode->appendChild($context->getNavigator()->accept($statusCode->get(), null, $context));

                $messageNode = $visitor->document->createElement('message');
                $visitor->getCurrentNode()->appendChild($messageNode);
                $messageNode->appendChild($context->getNavigator()->accept('Validation Failed', null, $context));

                $errorsNode = $visitor->document->createElement('errors');
                $visitor->getCurrentNode()->appendChild($errorsNode);
                $visitor->setCurrentNode($errorsNode);
                $this->serializeFormToXml($visitor, $form, $type);
                $visitor->revertCurrentNode();
                return;
            }
        }

        if (null === $visitor->document) {
            $visitor->document = $visitor->createDocument(null, null, false);
            $visitor->document->appendChild($formNode = $visitor->document->createElement('errors'));
            $visitor->setCurrentNode($formNode);
        } else {
            $formNode = $visitor->getCurrentNode();
        }

        $formNode->appendChild($errorsNode = $visitor->document->createElement('errors'));
        foreach ($form->getErrors() as $error) {
            $errorNode = $visitor->document->createElement('entry');
            $errorNode->appendChild($this->serializeFormErrorToXml($visitor, $error, array()));
            $errorsNode->appendChild($errorNode);
        }


        $formChildren = $form->all();
        if ($formChildren) {
            $formNode->appendChild($childrenNode = $visitor->document->createElement('children'));
            foreach ($formChildren as $child) {
                if ($child instanceof Form) {
                    $childrenNode->appendChild($childNode = $visitor->document->createElement($child->getName()));
                    $visitor->setCurrentNode($childNode);
                    if (null !== $node = $this->serializeFormToXml($visitor, $child, array())) {
                        $childrenNode->appendChild($node);
                    }
                }
            }
        }

        return $formNode;
    }
}
