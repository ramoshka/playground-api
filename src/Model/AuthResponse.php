<?php
/**
 * Created 2017-11-08 11:57
 */

declare(strict_types=1);


namespace App\Model;

use JMS\Serializer\Annotation as Serializer;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     definition="AuthRequest",
 *     @SWG\Xml(name="request"),
 *     @SWG\Property(property="username", type="string"),
 *     @SWG\Property(property="password", type="string"),
 * )
 */

/**
 * Class AuthResponse.
 *
 * This class helps to have serialized <result><token/><refreshToken/></result> response
 * instead of <result><entry/><entry/></result>. Considerations to move to App\Entity namespace.
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Model
 *
 * @Serializer\ExclusionPolicy("NONE")
 *
 * @SWG\Definition(@SWG\Xml(name="result"))
 */
class AuthResponse
{
    /**
     * @SWG\Property()
     * @var string Access token
     */
    protected $token;

    /**
     * @SWG\Property()
     *
     * @var string Refresh token to issue new access tokens
     */
    protected $refreshToken;

    /**
     * AuthResponse constructor.
     * @param string $token
     * @param string $refreshToken
     */
    public function __construct($token, $refreshToken)
    {
        $this->token = $token;
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }
}
