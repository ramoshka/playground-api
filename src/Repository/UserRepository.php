<?php
/**
 * Created 2017-11-02 13:05
 */

declare(strict_types=1);


namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Repository
 */
class UserRepository extends EntityRepository
{
    /**
     * Persist User to data storage.
     *
     * @param User $user
     * @return void
     */
    public function save(User $user)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }
}
