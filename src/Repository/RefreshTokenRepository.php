<?php
/**
 * Created 2017-11-07 11:48
 */

declare(strict_types=1);


namespace App\Repository;

use App\Entity\RefreshToken;
use Doctrine\ORM\EntityRepository;

/**
 * Class RefreshTokenRepository
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Repository
 */
class RefreshTokenRepository extends EntityRepository
{
    /**
     * Save refresh token.
     *
     * @param RefreshToken $token
     * @return void
     */
    public function save(RefreshToken $token)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($token);
        $entityManager->flush();
    }

    /**
     * Delete token from storage.
     *
     * @param RefreshToken $token
     * @return void
     */
    public function delete(RefreshToken $token)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($token);
        $entityManager->flush();
    }
}
