<?php
/**
 * Created 2017-11-15 14:12
 */

declare(strict_types=1);


namespace App\Repository;

use App\Entity\Role;
use Doctrine\ORM\EntityRepository;

/**
 * Class RoleRepository
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Repository
 */
class RoleRepository extends EntityRepository
{
    /**
     * Persist Role to data storage.
     *
     * @param Role $role
     * @return void
     */
    public function save(Role $role)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($role);
        $entityManager->flush();
    }
}
