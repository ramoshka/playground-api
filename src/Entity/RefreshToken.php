<?php
/**
 * Created 2017-11-07 11:46
 */

declare(strict_types=1);


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RefreshToken
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Entity
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @SWG\Definition()
 *
 * @ORM\Entity(repositoryClass="App\Repository\RefreshTokenRepository")
 * @ORM\Table(name="refresh_token")
 * @UniqueEntity("token")
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class RefreshToken
{
    /**
     * @Serializer\Expose()
     * @SWG\Property()
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer Unique identifier for token
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     *
     * @var string Encrypted token for user
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="refreshTokens")
     * @Assert\NotBlank()
     *
     * @var User Holder of the token
     */
    private $user;

    /**
     * @Serializer\Expose()
     * @SWG\Property()
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     *
     * @var \DateTime Token creation datetime
     */
    private $created;

    /**
     * @Serializer\Expose()
     * @SWG\Property()
     * @ORM\Column(type="string", length=45)
     * @Assert\NotBlank()
     * @Assert\Ip()
     *
     * @var string IP address from which refresh token was requested
     */
    private $ipAddress;

    /**
     * Browser/OS retrieved with `get_browser()`.
     *
     * @Serializer\Expose()
     * @SWG\Property()
     * @ORM\Column(type="string", length=255)
     *
     * @var string Browser data (and OS) extracted from user agent
     */
    private $browser;

    /**
     * RefreshToken constructor.
     * @param string $token
     * @param User $user
     * @param string $ipAddress
     * @param string $browser
     */
    public function __construct(string $token, User $user, string $ipAddress = null, string $browser = null)
    {
        $this->token = $token;
        $this->user = $user;
        $this->ipAddress = $ipAddress;
        $this->browser = $browser;
        $this->created = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RefreshToken
     */
    public function setId(int $id): RefreshToken
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @return string
     */
    public function getBrowser(): string
    {
        return $this->browser;
    }
}
