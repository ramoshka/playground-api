<?php
/**
 * Created 2017-11-15 10:33
 */

declare(strict_types=1);


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Role\Role as BaseRole;

/**
 * Class Group
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Entity
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @SWG\Definition()
 *
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 * @ORM\Table(name="role")
 * @UniqueEntity("role")
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Role extends BaseRole
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer Unique identifier
     */
    private $id;

    /**
     * @Serializer\Expose()
     * @SWG\Property()
     * @ORM\Column(type="string", unique=true)
     *
     * @var string Role code
     */
    private $role;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     *
     * @var ArrayCollection List of users with given role
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="Role", mappedBy="parent")
     *
     * @var ArrayCollection Roles below current one in role hierarchy
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="children")
     *
     * @var Role Parent role
     */
    private $parent;

    /**
     * Group constructor.
     *
     * @param string $role
     */
    public function __construct(string $role)
    {
        $this->role = $role;
        $this->users = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Role
     */
    public function setId(int $id): Role
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers(): ArrayCollection
    {
        return $this->users;
    }

    /**
     * @param User $user
     * @return Role
     */
    public function addUser(User $user): Role
    {
        $this->users->add($user);
        return $this;
    }

    /**
     * @param Role $role
     * @return Role
     */
    public function setParent(Role $role): Role
    {
        $this->parent = $role;
        return $this;
    }

    /**
     * @return Role|null
     */
    public function getParent()
    {
        return $this->parent;
    }


    /**
     * @return ArrayCollection
     */
    public function getChildren(): ArrayCollection
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->role;
    }
}
