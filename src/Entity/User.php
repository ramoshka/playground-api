<?php
/**
 * Created 2017-10-31 15:59
 */

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Entity
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @SWG\Definition()
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity("username")
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class User implements UserInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer Unique user identifier used internally
     */
    private $id;

    /**
     * @Serializer\Expose()
     * @SWG\Property(uniqueItems=true)
     * @ORM\Column(type="string", unique=true, length=255)
     * @Assert\NotBlank()
     *
     * @var string Unique identifier for user
     */
    private $username;

    /**
     * @Serializer\Expose()
     * @SWG\Property(format="email")
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     * @var string User contact email
     */
    private $email;

    /**
     * bcrypt hashes are 64 characters length.
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     *
     * @var string bcrypt'ed password
     */
    private $password;

    /**
     * @Serializer\Expose()
     * @Serializer\Accessor(getter="isActive", setter="setIsActive")
     * @SWG\Property()
     * @ORM\Column(type="boolean")
     *
     * @var boolean Is user active
     */
    private $isActive;

    /**
     * @Serializer\Expose()
     * @SWG\Property()
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime User resource creation datetime
     */
    private $created;

    /**
     * @Serializer\Expose()
     * @SWG\Property()
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime Datetime of last modification
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity="RefreshToken", mappedBy="user")
     *
     * @var ArrayCollection Refresh tokens tah user holds
     */
    private $refreshTokens;

    /**
     * @SWG\Property()
     * @ORM\Column(type="integer", options={"default": 0})
     *
     * @var int Number of failed login attempts in a row
     */
    private $failedLoginCount = 0;

    /**
     * @Serializer\Expose()
     * @SWG\Property()
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime|null
     */
    private $suspendedUntil = null;

    /**
     * @Serializer\Expose()
     * @Serializer\MaxDepth(depth=2)
     * @SWG\Property()
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     *
     * @var ArrayCollection Groups user is in
     */
    private $roles;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->isActive = false;
        $this->updated = new \DateTime();
        $this->created = new \DateTime();
        $this->refreshTokens = new ArrayCollection();
        $this->failedLoginCount = 0;
        $this->roles = new ArrayCollection();
    }

    /**
     * @return integer|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return User
     */
    public function setId(string $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return User
     */
    public function setIsActive(bool $isActive): User
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return User
     */
    public function setCreated(\DateTime $created): User
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     * @return User
     */
    public function setUpdated(\DateTime $updated): User
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRefreshTokens()
    {
        return $this->refreshTokens;
    }

    /**
     * @Serializer\VirtualProperty()
     *
     * @return int
     */
    public function getFailedLoginCount(): int
    {
        return $this->failedLoginCount;
    }

    /**
     * Set that user failed to authenticate.
     *
     * @return User
     */
    public function incrementFailedLoginCount(): User
    {
        $this->failedLoginCount += 1;
        return $this;
    }

    /**
     * Reset failed login attempts counter.
     *
     * @return User
     */
    public function resetFailedLoginCount(): User
    {
        $this->failedLoginCount = 0;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getSuspendedUntil()
    {
        return $this->suspendedUntil;
    }

    /**
     * Set new suspension date for user.
     *
     * @param \DateTime $suspendedUntil
     * @return User
     */
    public function setSuspendedUntil(\DateTime $suspendedUntil): User
    {
        $this->suspendedUntil = $suspendedUntil;
        return $this;
    }

    /**
     * Get user roles.
     *
     * @return Role[]
     */
    public function getRoles(): array
    {
        return $this->roles->toArray();
    }

    /**
     * Return salt if used. bcrypt doesn't use predefined salt.
     *
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return void
     */
    public function eraseCredentials()
    {
    }

    /**
     * Checks whether the user can login.
     *
     * @return bool true if the user is enabled, false otherwise
     */
    public function canLogin(): bool
    {
        if (!$this->suspendedUntil) {
            return $this->isActive;
        }
        return (new \DateTime() > $this->suspendedUntil) && $this->isActive;
    }

    /**
     * Return string representation of user entity.
     *
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s %s', self::class, $this->getUsername());
    }
}
