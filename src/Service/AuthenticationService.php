<?php
/**
 * Created 05/11/17 18:47
 */

declare(strict_types=1);


namespace App\Service;

use App\Entity\RefreshToken;
use App\Entity\User;
use App\Exception\JwtException;
use App\Model\AuthResponse;
use App\Repository\RefreshTokenRepository;
use App\Repository\UserRepository;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class JwtProvider
 *
 * @author Irmantas Ramoška <irmantas@ramoska.eu>
 * @package App\Service
 */
class AuthenticationService
{
    const SUPPORTED_ALGS = ['RS512'];
    const RANDOM_BYTES = 64;
    const SUSPENSION_PERIODS = [
        3 => '+5 minutes',
        5 => '+30 minutes',
        8 => '+1 day',
        10 => '+30 days',
    ];

    /**
     * @var string
     */
    private $publicKey;

    /**
     * @var string
     */
    private $privateKey;

    /**
     * @var RefreshTokenRepository
     */
    private $tokenRepository;

    /**
     * @var PasswordEncoderInterface
     */
    private $tokenEncoder;

    /**
     * @var \stdClass
     */
    private $jwt;

    /**
     * @var UserRepository
     */
    private $userRepository;


    /**
     * AuthenticationService constructor.
     *
     * @param string $publicKey
     * @param string $privateKey
     * @param RefreshTokenRepository $tokenRepository
     * @param PasswordEncoderInterface $encoder
     * @param UserRepository $userRepository
     */
    public function __construct(
        string $publicKey,
        string $privateKey,
        RefreshTokenRepository $tokenRepository,
        PasswordEncoderInterface $encoder,
        UserRepository $userRepository
    ) {
        $this->publicKey = file_get_contents($publicKey);
        $this->privateKey = file_get_contents($privateKey);
        $this->tokenRepository = $tokenRepository;
        $this->tokenEncoder = $encoder;
        $this->userRepository = $userRepository;
        $this->jwt = new JWT();
    }

    /**
     * @param UserInterface $user
     * @param int $tokenValidity
     * @return string
     */
    public function createJwtToken(UserInterface $user, int $tokenValidity = 1200): string
    {
        /** @var User $user */
        $iat = time();
        $payload = [
            'username' => $user->getUsername(),
            'roles' => array_map(function($role) { return $role->getRole(); }, $user->getRoles()),
            'iat' => $iat,
            'exp' => $iat + $tokenValidity,
        ];
        $token = $this->jwt::encode($payload, $this->privateKey, array_values(self::SUPPORTED_ALGS)[0]);
        return $token;
    }

    /**
     * @param string $token
     * @return array
     *
     * @throws JwtException
     */
    public function decodeToken(string $token): array
    {
        try {
            $token = $this->jwt::decode($token, $this->publicKey, self::SUPPORTED_ALGS);
        } catch (\UnexpectedValueException $e) {
            throw new JwtException($e->getMessage());
        }
        return (array)$token;
    }

    /**
     * @param UserInterface $user
     * @param Request $request
     * @return AuthResponse
     */
    public function loginUser(UserInterface $user, Request $request): AuthResponse
    {
        /** @var User $user */
        if ($user->getFailedLoginCount()) {
            $user->resetFailedLoginCount();
            $this->userRepository->save($user);
        }

        $browser = get_browser($request->headers->get('User-Agent'));
        $browserStr = 'Other';
        if ($browser->browser && $browser->platform) {
            $browserStr = sprintf('%s on %s', $browser->browser, $browser->platform);
        } elseif ($browser->browser) {
            $browserStr = $browser->browser;
        } elseif ($browser->platform) {
            $browserStr = $browser->platform;
        }

        $result = new AuthResponse(
            $this->createJwtToken($user),
            bin2hex(openssl_random_pseudo_bytes(self::RANDOM_BYTES))
        );

        $this->tokenRepository->save(new RefreshToken(
            $this->tokenEncoder->encodePassword($result->getRefreshToken(), null),
            $user,
            $request->getClientIp(),
            $browserStr
        ));

        return $result;
    }

    /**
     * Issue new access token based on unencrypted refresh token.
     *
     * Possible enhancement may be done by regenerating refresh token.
     *
     * @param string $refreshToken
     * @return AuthResponse
     */
    public function reissueAccessToken(string $refreshToken): AuthResponse
    {
        $encodedToken = $this->tokenEncoder->encodePassword($refreshToken, null);
        /** @var RefreshToken $dbToken */
        $dbToken = $this->tokenRepository->findOneBy(['token' => $encodedToken]);
        if (!$dbToken) {
            throw new AuthenticationCredentialsNotFoundException();
        }
        $user = $dbToken->getUser();
        $user->getRoles();
        return new AuthResponse($this->createJwtToken($user), $refreshToken);
    }

    /**
     * Suspend user account if necessary after failed login attempt.
     *
     * @param UserInterface $user
     * @return void
     */
    public function punishFailedLogin(UserInterface $user)
    {
        /** @var User $user */
        $user->incrementFailedLoginCount();
        $failedLoginCount = $user->getFailedLoginCount();
        if (array_key_exists($failedLoginCount, self::SUSPENSION_PERIODS)) {
            $user->setSuspendedUntil(new \DateTime(self::SUSPENSION_PERIODS[$failedLoginCount]));
        }
        $this->userRepository->save($user);
    }

    /**
     * Delete refresh token from storage.
     *
     * @param string $refreshToken
     * @param User $user
     * @return void
     */
    public function deleteRefreshToken(string $refreshToken, User $user)
    {
        $encodedToken = $this->tokenEncoder->encodePassword($refreshToken, null);
        /** @var RefreshToken $dbToken */
        $dbToken = $this->tokenRepository->findOneBy(['token' => $encodedToken]);
        if (!$dbToken) {
            throw new AuthenticationCredentialsNotFoundException();
        }
        if ($dbToken->getUser()->getId() != $user->getId()) {
            throw new AuthenticationCredentialsNotFoundException();
        }
        $this->tokenRepository->delete($dbToken);
        return;
    }
}
