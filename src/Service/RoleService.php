<?php
/**
 * Created 2017-11-15 14:38
 */

declare(strict_types=1);


namespace App\Service;

use App\Entity\Role;
use App\Repository\RoleRepository;
use Psr\SimpleCache\CacheInterface;

/**
 * Class RoleService
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Service
 */
class RoleService
{
    const CACHE_ROLES = 'role_service.roles';
    const CACHE_ROLE_TREE = 'role_service.role_tree';

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * RoleService constructor.
     * @param CacheInterface $cache
     * @param RoleRepository $roleRepository
     */
    public function __construct(CacheInterface $cache, RoleRepository $roleRepository)
    {
        $this->cache = $cache;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param Role $role
     */
    public function createRole(Role $role)
    {
        $this->roleRepository->save($role);
        $this->cache->deleteMultiple([self::CACHE_ROLES, self::CACHE_ROLE_TREE]);
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        if ($this->cache->has(self::CACHE_ROLES)) {
            return $this->cache->get(self::CACHE_ROLES);
        }

        $roles = $this->roleRepository->findAll();
        $this->cache->set(self::CACHE_ROLES, $roles);
        return $roles;
    }

    /**
     * @return array
     *
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    public function getRoleTree(): array
    {
        if ($this->cache->has(self::CACHE_ROLE_TREE)) {
            return $this->cache->get(self::CACHE_ROLE_TREE);
        }

        $tree = [];
        $roles = $this->getRoles();
        /** @var Role $role */
        foreach ($roles as $role) {
            if ($role->getParent()) {
                $parentRole = $role->getParent()->getRole();
                if (!isset($tree[$parentRole])) {
                    $tree[$parentRole] = [];
                }
                $tree[$parentRole][] = $role->getRole();
            } else {
                $tree[$role->getRole()] = [];
            }
        }
        $this->cache->set(self::CACHE_ROLE_TREE, $tree);
        return $tree;
    }
}
