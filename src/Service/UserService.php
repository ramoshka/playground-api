<?php
/**
 * Created 2017-11-02 11:17
 */

declare(strict_types=1);


namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserService
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Service
 */
class UserService
{
    /**
     * @var UserRepository $userRepository
     */
    private $userRepository;

    /**
     * @var UserPasswordEncoderInterface $passwordEncoder
     */
    private $passwordEncoder;

    /**
     * UserService constructor.
     *
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Create user and instruct repository manager to persist data.
     *
     * @param User $user
     * @return User
     */
    public function createUser(User $user): User
    {
        $now = new \DateTime();
        $user
            ->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()))
            ->setIsActive(false)
            ->setCreated($now)
            ->setUpdated($now);
        $this->userRepository->save($user);
        return $user;
    }

    /**
     * Retrieve user by username.
     *
     * @param string $username
     * @return User
     *
     * @throws NotFoundHttpException
     */
    public function getByUsername(string $username): User
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['username' => $username]);
        if (!$user) {
            throw new NotFoundHttpException();
        }
        return $user;
    }

    /**
     * Get all users.
     *
     * @param int $page
     * @param int $pageSize
     * @return Paginator
     */
    public function getAll(int $page, int $pageSize): Paginator
    {
        $query = $this->userRepository
            ->createQueryBuilder('user')
            ->setFirstResult($pageSize * ($page - 1))
            ->setMaxResults($pageSize);
        return new Paginator($query);
    }
}
