<?php
/**
 * Created 2017-11-07 10:39
 */

declare(strict_types=1);


namespace App\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Class JwtException
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Exception
 */
class JwtException extends AuthenticationException
{

}
