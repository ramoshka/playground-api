<?php
/**
 * Created 2017-11-03 09:04
 */

declare(strict_types=1);


namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Swagger\Annotations as SWG;

/**
 * Class UserType
 *
 * @SWG\Definition(
 *     definition="UserType",
 *     description="User creation form",
 *     @SWG\Xml(name="user"),
 *     @SWG\Property(
 *         property="username",
 *         type="string",
 *         uniqueItems=true,
 *         description="Username by which user will be identified in the future requests"
 *     ),
 *     @SWG\Property(property="password", type="string", description="Should be strong password"),
 *     @SWG\Property(
 *         property="email",
 *         type="string",
 *         format="email",
 *         description="Email by which user will be contacted in the future"
 *     )
 * )
 *
 * @SWG\Definition(
 *     definition="FormErrors",
 *     @SWG\Xml(name="result"),
 *     @SWG\Property(
 *         property="code",
 *         type="integer",
 *         example=400,
 *         description="Response status code, almost always `400`"
 *     ),
 *     @SWG\Property(
 *         property="message",
 *         type="string",
 *         description="Validation error message",
 *         example="Validation Failed"
 *     ),
 *     @SWG\Property(
 *         property="errors",
 *         description="Form validation errors",
 *         @SWG\Property(
 *             property="errors",
 *             type="array",
 *             description="Global form errors",
 *             @SWG\Xml(wrapped=true),
 *             @SWG\Items(type="string", description="Single error entry", @SWG\Xml(name="entry"))
 *         ),
 *         @SWG\Property(
 *             property="children",
 *             description="Form field validation errors",
 *             @SWG\Property(
 *                 property="fieldName",
 *                 description="Single field errors",
 *                 @SWG\Property(
 *                     property="errors",
 *                     type="array",
 *                     description="All errors for field",
 *                     @SWG\Xml(wrapped=true),
 *                     @SWG\Items(type="string", description="Single error entry", @SWG\Xml(name="entry"))
 *                 )
 *             )
 *         )
 *     )
 * )
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Form
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class)
            ->add('password', PasswordType::class)
            ->add('email', EmailType::class);
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
        ]);
    }
}
