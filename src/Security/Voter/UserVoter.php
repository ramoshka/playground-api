<?php
/**
 * Created 2017-11-16 11:36
 */

declare(strict_types=1);


namespace App\Security\Voter;

use App\Security\Role\RoleHierarchy;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class UserCollectionVoter
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Security\Voter
 */
class UserVoter extends Voter
{
    const LIST_USERS = 'list_users';
    const CREATE_USER = 'create_user';
    const ROLES_CREATE = ['ROLE_SUPER_ADMIN', 'ROLE_ORG_ADMIN'];
    const ROLES_LIST = ['ROLE_SUPER_ADMIN', 'ROLE_ORG_ADMIN'];

    /**
     * @var RoleHierarchy
     */
    private $roleHierarchy;

    /**
     * UserVoter constructor.
     * @param RoleHierarchy $roleHierarchy
     */
    public function __construct(RoleHierarchy $roleHierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function supports($attribute, $subject)
    {
        if (in_array($attribute, [self::CREATE_USER, self::LIST_USERS])) {
            return true;
        }
        return false;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $roles = $this->roleHierarchy->getReachableRoles($token->getRoles());
        if ($attribute == self::LIST_USERS) {
            foreach ($roles as $role) {
                if (in_array($role->getRole(), self::ROLES_LIST)) {
                    return true;
                }
            }
        }
        if ($attribute == self::CREATE_USER) {
            foreach ($roles as $role) {
                if (in_array($role->getRole(), self::ROLES_CREATE)) {
                    return true;
                }
            }
        }
        return false;
    }
}
