<?php
/**
 * Created 2017-11-07 18:37
 */

declare(strict_types=1);


namespace App\Security\Encoder;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * Class RefreshTokenEncoder
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Security\Encoder
 */
class RefreshTokenEncoder implements PasswordEncoderInterface
{
    const ENCRYPTION_METHOD = 'aes256';
    /**
     * @var string $key
     */
    private $key;

    /**
     * @var string $initializationVector
     */
    private $initializationVector;

    /**
     * RefreshTokenEncoder constructor.
     * @param string $key
     * @param string $initializationVector
     */
    public function __construct(string $key, string $initializationVector)
    {
        $this->key = $key;
        $this->initializationVector = $initializationVector;
    }

    /**
     * Encodes the raw password.
     *
     * @param string $raw The password to encode
     * @param string $salt The salt
     * @return string The encoded password
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function encodePassword($raw, $salt = null): string
    {
        return openssl_encrypt($raw, self::ENCRYPTION_METHOD, $this->key, 0, $this->initializationVector);
    }

    /**
     * Checks a raw password against an encoded password.
     *
     * @param string $encoded An encoded password
     * @param string $raw A raw password
     * @param string $salt The salt
     * @return bool true if the password is valid, false otherwise
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function isPasswordValid($encoded, $raw, $salt = null): bool
    {
        return openssl_encrypt($raw, self::ENCRYPTION_METHOD, $this->key, 0, $this->initializationVector) == $encoded;
    }

}
