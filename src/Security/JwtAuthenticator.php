<?php
/**
 * Created 05/11/17 18:07
 */

declare(strict_types=1);


namespace App\Security;

use App\Exception\JwtException;
use App\Service\AuthenticationService;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Guard\AuthenticatorInterface;

/**
 * Class JwtAuthenticator
 *
 * @author Irmantas Ramoška <irmantas@ramoska.eu>
 * @package App\Security
 */
class JwtAuthenticator extends AbstractGuardAuthenticator
{
    const AUTHORIZATION = 'Bearer';

    /**
     * @var AuthenticationService $authService
     */
    private $authService;

    /**
     * JwtAuthenticator constructor.
     *
     * @param AuthenticationService $authService
     */
    public function __construct(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request)
    {
        return $request->headers->get('Authorization', null) !== null;
    }

    /**
     * Get the authentication credentials from the request.
     * If null is returned, authentication will be skipped.
     *
     * Whatever value is returned here, it will be passed to getUser() and checkCredentials().
     *
     * @param Request $request
     * @return array|bool
     */
    public function getCredentials(Request $request)
    {
        $token = $this->extractToken($request);
        if (!$token) {
            return false;
        }
        return ['token' => $token];
    }

    /**
     * Return a UserInterface object based on the credentials.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * You may throw an AuthenticationException if you wish. If you return
     * null, then a UsernameNotFoundException is thrown for you.
     *
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface|null
     *
     * @throws BadCredentialsException
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            $token = $this->authService->decodeToken($credentials['token']);
        } catch (JwtException $e) {
            throw new UnauthorizedHttpException(self::AUTHORIZATION, $e->getMessage());
        } catch (\Exception $e) {
            throw new UnauthorizedHttpException(self::AUTHORIZATION, $e->getMessage());
        }
        $user = $userProvider->loadUserByUsername($token['username']);
        return $user;
    }

    /**
     * Returns true if the credentials are valid.
     *
     * If any value other than true is returned, authentication will
     * fail.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return null
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        throw new UnauthorizedHttpException(self::AUTHORIZATION, $exception->getMessage());
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If null is returned, the current request will continue, and the user
     * will be authenticated. This makes sense with an API.
     *
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return Response|null
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * Does this method support remember me cookies?
     *
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * This is called when an anonymous request accesses a resource that
     * requires authentication. The job of this method is to return some
     * response that "helps" the user start into the authentication process.
     *
     * Examples:
     *  A) For a form login, you might redirect to the login page
     *      return new RedirectResponse('/login');
     *  B) For an API token authentication system, you return a 401 response
     *      return new Response('Auth header required', 401);
     *
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @throws LogicException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        throw new LogicException();
    }

    /**
     * Extract JWT from request headers. Looking for 'Authorization: Bearer <token>' header.
     *
     * @param Request $request
     * @return null|string
     */
    private function extractToken(Request $request)
    {
        $header = $request->headers->get('Authorization');
        if (!$header) {
            return null;
        }

        $parts = explode(' ', $header);
        if (count($parts) !== 2) {
            return null;
        }

        if ($parts[0] !== self::AUTHORIZATION) {
            return null;
        }

        return $parts[1];
    }
}
