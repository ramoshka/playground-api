<?php
/**
 * Created 2017-11-06 14:38
 */

declare(strict_types=1);


namespace App\Security;

use App\Entity\User;
use App\Service\AuthenticationService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Jms\Serializer\Serializer;

/**
 * Class LoginAuthenticator
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Security
 */
class LoginAuthenticator extends AbstractGuardAuthenticator
{
    const MAX_PASS_LENGTH = 1024;
    const BAD_CREDENTIALS = 'User with provided credentials is not found';
    const BAD_REQUEST = 'Request does not contain valid username and/or password';
    const AUTHENTICATION_REQUIRED = 'Authentication required';
    const CHALLENGE = 'Bearer';

    /**
     * @var UserPasswordEncoderInterface $passwordEncoder
     */
    protected $passwordEncoder;

    /**
     * @var AuthenticationService $authService
     */
    protected $authService;

    /**
     * @var Serializer $serializer
     */
    protected $serializer;

    /**
     * LoginAuthenticator constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param AuthenticationService $authService
     * @param Serializer $serializer
     */
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        AuthenticationService $authService,
        Serializer $serializer
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->authService = $authService;
        $this->serializer = $serializer;
    }

    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request)
    {
        return $request->headers->get('Authorization', null) === null;
    }


    /**
     * Get the authentication credentials from the request and return them as any type (e.g. an associate array).
     * If null is returned, authentication will be skipped.
     *
     * Whatever value you return here will be passed to getUser() and checkCredentials()
     *
     * @param Request $request
     * @return mixed
     *
     * @throws MethodNotAllowedHttpException
     * @throws BadRequestHttpException
     */
    public function getCredentials(Request $request)
    {
        if ($request->getPathInfo() != '/api/auth/login') {
            return false;
        }
        if (!$request->isMethod('POST')) {
            throw new MethodNotAllowedHttpException(['POST']);
        }

        $password = $request->request->get('password');
        $username = $request->request->get('username');
        if (!$username || !$password) {
            throw new BadRequestHttpException(self::BAD_REQUEST);
        }
        if (strlen($password) > self::MAX_PASS_LENGTH) {
            throw new BadRequestHttpException(self::BAD_REQUEST);
        }
        return compact('username', 'password');
    }

    /**
     * Return a UserInterface object based on the credentials.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * You may throw an AuthenticationException if you wish. If you return
     * null, then a UsernameNotFoundException is thrown for you.
     *
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface
     *
     * @throws BadRequestHttpException
     */
    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface
    {
        $user = $userProvider->loadUserByUsername($credentials['username']);
        if (!$user) {
            throw new UnauthorizedHttpException(self::CHALLENGE, self::BAD_CREDENTIALS);
        }
        return $user;
    }

    /**
     * Returns true if the credentials are valid.
     *
     * If any value other than true is returned, authentication will
     * fail. You may also throw an AuthenticationException if you wish
     * to cause authentication to fail.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     *
     * @throws BadRequestHttpException
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        /** @var User $user */
        if (!$user->canLogin()) {
            throw new UnauthorizedHttpException(self::CHALLENGE, self::BAD_CREDENTIALS);
        }
        if ($this->passwordEncoder->isPasswordValid($user, $credentials['password'])) {
            return true;
        }
        $this->authService->punishFailedLogin($user);
        throw new UnauthorizedHttpException(self::CHALLENGE, self::BAD_CREDENTIALS);
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     * @return void
     *
     * @throws BadRequestHttpException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        throw new UnauthorizedHttpException(self::CHALLENGE, self::BAD_CREDENTIALS);
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     * @return Response|null
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): Response
    {
        $result = $this->authService->loginUser($token->getUser(), $request);
        $intersection = array_intersect($request->getAcceptableContentTypes(), ['application/xml', 'text/xml']);
        $contentType = 'application/json';
        $format = 'json';
        if ($intersection) {
            $format = 'xml';
            $contentType = $intersection[0];
        }
        return new Response(
            $this->serializer->serialize($result, $format),
            Response::HTTP_OK,
            ['Content-Type' => $contentType]
        );
    }

    /**
     * Does this method support remember me cookies?
     *
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     * @return void
     *
     * @throws UnauthorizedHttpException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        throw new UnauthorizedHttpException(self::CHALLENGE, self::AUTHENTICATION_REQUIRED);
    }
}
