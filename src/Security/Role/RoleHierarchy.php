<?php
/**
 * Created 2017-11-15 15:43
 */

declare(strict_types=1);


namespace App\Security\Role;

use App\Service\RoleService;
use App\Entity\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * Class RoleHierarchy
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Security\Role
 */
class RoleHierarchy implements RoleHierarchyInterface
{
    /**
     * @var RoleService
     */
    private $roleService;

    /**
     * @var array
     */
    private $map;

    /**
     * RoleHierarchy constructor.
     * @param RoleService $roleService
     */
    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
        $this->buildRoleMap($this->roleService->getRoleTree());
    }


    /**
     * Returns an array of all reachable roles by the given ones.
     *
     * Reachable roles are the roles directly assigned but also all roles that
     * are transitively reachable from them in the role hierarchy.
     *
     * @param Role[] $roles An array of directly assigned roles
     *
     * @return Role[] An array of all reachable roles
     */
    public function getReachableRoles(array $roles)
    {
        $reachableRoles = $roles;
        foreach ($roles as $role) {
            if (!isset($this->map[$role->getRole()])) {
                continue;
            }
            foreach ($this->map[$role->getRole()] as $r) {
                $reachableRoles[] = new Role($r);
            }
        }
        return $reachableRoles;
    }

    /**
     * Build role map.
     *
     * @param array $hierarchy
     * @return void
     */
    private function buildRoleMap(array $hierarchy)
    {
        $this->map = [];
        foreach ($hierarchy as $main => $roles) {
            $this->map[$main] = $roles;
            $visited = [];
            $additionalRoles = $roles;
            while ($role = array_shift($additionalRoles)) {
                if (!isset($hierarchy[$role])) {
                    continue;
                }

                $visited[] = $role;

                foreach ($hierarchy[$role] as $roleToAdd) {
                    $this->map[$main][] = $roleToAdd;
                }

                foreach (array_diff($hierarchy[$role], $visited) as $additionalRole) {
                    $additionalRoles[] = $additionalRole;
                }
            }

            $this->map[$main] = array_unique($this->map[$main]);
        }
    }
}
