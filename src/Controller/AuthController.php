<?php
/**
 * Created 05/11/17 19:24
 */

declare(strict_types=1);


namespace App\Controller;

use App\Model\AuthResponse;
use App\Service\AuthenticationService;
use App\Service\UserService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;

/**
 * Class AuthController
 *
 * @author Irmantas Ramoška <irmantas@ramoska.eu>
 * @package App\Controller
 *
 * @SWG\Swagger(
 *     consumes={"application/json", "application/xml"},
 *     produces={"application/json", "application/xml"},
 *     @SWG\Info(title="Symfony", version="0.0.1"),
 *     @SWG\SecurityScheme(type="apiKey", name="Authorization", in="header", securityDefinition="AccessToken")
 * )
 *
 * @SWG\Definition(
 *     definition="ErrorResponse",
 *     @SWG\Xml(name="result"),
 *     @SWG\Property(property="code", type="integer"),
 *     @SWG\Property(property="message", type="string")
 * )
 *
 * @Rest\Route("/auth")
 */
class AuthController extends FOSRestController
{
    /**
     * @var AuthenticationService $authService
     */
    private $authService;

    /**
     * @var UserService $userService
     */
    private $userService;

    /**
     * AuthController constructor.
     *
     * @param AuthenticationService $authService
     * @param UserService $userService
     */
    public function __construct(AuthenticationService $authService, UserService $userService)
    {
        $this->authService = $authService;
        $this->userService = $userService;
    }

    /**
     * Authenticate user.
     *
     * @SWG\Post(
     *     path="/api/auth/login",
     *     tags={"Auth"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="credentials",
     *         description="User credentials",
     *         @SWG\Schema(ref="#/definitions/AuthRequest")
     *     ),
     *     @SWG\Response(
     *         description="Successful authentication",
     *         response="200",
     *         @SWG\Schema(ref="#/definitions/AuthResponse")
     *     ),
     *     @SWG\Response(
     *         description="Malformed request",
     *         response="400",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     ),
     *     @SWG\Response(
     *         description="Authentication failed",
     *         response="401",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     )
     * )
     *
     * @Rest\Post("/login", name="authentication_login")
     * @Rest\View
     *
     * @internal
     */
    public function loginAction()
    {
        // This should never be called since login action is handled by LoginAuthenticator guard
        throw new \LogicException();
    }

    /**
     * Get new JWT token if valid refresh token is provided.
     *
     * @Rest\Post("/refresh-token", name="authentication_refresh")
     * @Rest\RequestParam(name="refreshToken", allowBlank=false, nullable=false)
     * @Rest\View(populateDefaultVars=false)
     *
     * @SWG\Post(
     *     path="/api/auth/refresh-token",
     *     tags={"Auth"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="refreshToken",
     *         description="Refresh token",
     *         @SWG\Schema(
     *             @SWG\Xml(name="request"),
     *             @SWG\Property(property="refreshToken", type="string", @SWG\Xml(name="refreshToken"))
     *         )
     *     ),
     *     @SWG\Response(
     *         description="Successful authentication",
     *         response="200",
     *         @SWG\Schema(ref="#/definitions/AuthResponse")
     *     ),
     *     @SWG\Response(
     *         description="Authentication failed",
     *         response="401",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     )
     * )
     *
     * @param ParamFetcherInterface $params
     * @return AuthResponse
     */
    public function refreshTokenAction(ParamFetcherInterface $params)
    {
        $token = $params->get('refreshToken');
        return $this->authService->reissueAccessToken($token);
    }

    /**
     * Delete refresh token.
     *
     * @Rest\Delete("/refresh-token/{token}", name="authentication_delete_refresh_token")
     * @Rest\View(populateDefaultVars=false)
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @SWG\Delete(
     *     path="/api/auth/refresh-token/{token}",
     *     tags={"Auth"},
     *     security={{"AccessToken"={},}},
     *     @SWG\Parameter(
     *         in="query",
     *         name="token",
     *         description="Refresh token",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         description="Successfully deleted token",
     *         response="204"
     *     ),
     *     @SWG\Response(
     *         description="Authentication failed",
     *         response="401",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     )
     * )
     *
     * @param string $token
     * @return void
     */
    public function deleteRefreshTokenAction(string $token)
    {
        $this->authService->deleteRefreshToken($token, $this->getUser());
    }
}
