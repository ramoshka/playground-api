<?php
/**
 * Created 2017-10-30 16:01
 */

declare(strict_types=1);


namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\UserService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserController
 *
 * @author Irmantas Ramoška <irmantas.ramoska@megodata.com>
 * @package App\Controller
 *
 * @Rest\Route("users")
 */
class UserController extends FOSRestController
{
    /**
     * @var UserService $userService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Get users list.
     *
     * @Rest\Get("", name="users_list")
     * @Rest\QueryParam(
     *     name="page",
     *     default=1,
     *     requirements=@Assert\Range(min=1),
     *     description="Page number for paged responses",
     *     strict=true,
     *     nullable=true
     * )
     * @Rest\QueryParam(
     *     name="pageSize",
     *     default=20,
     *     requirements=@Assert\Range(min=1, max=30),
     *     description="Number of entries in single page",
     *     strict=true,
     *     nullable=true
     * )
     * @Rest\View(serializerEnableMaxDepthChecks=true, populateDefaultVars=false)
     *
     * @Security("is_granted('list_users')")
     *
     * @SWG\Get(
     *     path="/api/users",
     *     tags={"Users"},
     *     security={{"AccessToken"={},}},
     *     @SWG\Parameter(
     *         name="page",
     *         type="integer",
     *         default="1",
     *         description="Page number for paged responses",
     *         in="query",
     *         allowEmptyValue=true,
     *         required=false
     *     ),
     *     @SWG\Parameter(
     *         name="pageSize",
     *         type="integer",
     *         default="30",
     *         description="number of entries in single page",
     *         in="query",
     *         allowEmptyValue=true,
     *         required=false,
     *         minimum="1",
     *     ),
     *     @SWG\Response(
     *         description="Successful authentication",
     *         response="200",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Xml(name="entry", wrapped=true),
     *             @SWG\Items(ref="#/definitions/User", @SWG\Xml(name="entry"), minItems=0)
     *         )
     *     ),
     *     @SWG\Response(
     *         description="Bad request",
     *         response="400",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     ),
     *     @SWG\Response(
     *         description="Authentication failed",
     *         response="401",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     )
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return mixed
     */
    public function indexAction(ParamFetcherInterface $paramFetcher)
    {
        $users = $this->userService->getAll((int)$paramFetcher->get('page'), (int)$paramFetcher->get('pageSize'));
        return iterator_to_array($users);
    }

    /**
     * Create new User resource.
     *
     * @Rest\Post("", name="users_create")
     * @Rest\View(serializerEnableMaxDepthChecks=true, populateDefaultVars=false)
     *
     * @Security("is_granted('create_user')")
     *
     * @SWG\Post(
     *     path="/api/users",
     *     tags={"Users"},
     *     security={{"AccessToken"={},}},
     *     @SWG\Response(
     *         description="User created successfully",
     *         response="204",
     *         @SWG\Header(header="Link", type="string", format="url", description="Link to new resource")
     *     ),
     *     @SWG\Response(
     *         description="Data validation failed",
     *         response="400",
     *         @SWG\Schema(ref="#/definitions/FormErrors")
     *     ),
     *     @SWG\Response(
     *         description="Authentication failed",
     *         response="401",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     )
     * )
     *
     * @param Request $request
     * @return View|FormInterface
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->userService->createUser($user);
            $view = $this->view();
            $view->setHeader('link', $this->generateUrl('users_details', ['username' => $user->getUsername()]));
            return $view;
        }
        return $form;
    }

    /**
     * Get single User resource.
     *
     * @Rest\Get("/{username}", name="users_details")
     * @Rest\View(serializerEnableMaxDepthChecks=true, populateDefaultVars=false)
     *
     * @Security("is_granted('list_users')")
     *
     * @SWG\Get(
     *     path="/api/users/{username}",
     *     tags={"Users"},
     *     security={{"AccessToken"={},}},
     *     @SWG\Response(
     *         description="User found",
     *         response="200",
     *         @SWG\Schema(
     *             @SWG\Xml(name="result"),
     *             ref="#/definitions/User"
     *         )
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         description="Username",
     *         name="username",
     *         in="path"
     *     ),
     *     @SWG\Response(
     *         description="Authentication failed",
     *         response="401",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     ),
     *     @SWG\Response(
     *         description="User not found",
     *         response="404",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     )
     * )
     *
     * @param string $username
     * @return User
     *
     * @throws NotFoundHttpException
     */
    public function detailsAction(string $username): User
    {
        return $this->userService->getByUsername($username);
    }

    /**
     * Get user's refresh tokens.
     *
     * @Rest\Get("/{username}/refresh-tokens", name="users_refresh_tokens")
     * @Rest\View(serializerEnableMaxDepthChecks=true, populateDefaultVars=false)
     *
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @SWG\Get(
     *     path="/api/users/{username}/refresh-tokens",
     *     tags={"Users"},
     *     security={{"AccessToken"={},}},
     *     @SWG\Response(
     *         description="List of refresh tokens",
     *         response="200",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Xml(name="entry", wrapped=true),
     *             @SWG\Items(ref="#/definitions/RefreshToken")
     *         )
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         description="Username",
     *         name="username",
     *         in="path"
     *     ),
     *     @SWG\Response(
     *         description="Authentication failed",
     *         response="401",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     ),
     *     @SWG\Response(
     *         description="User not found",
     *         response="404",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse")
     *     )
     * )
     *
     * @param string $username
     * @return \Doctrine\Common\Collections\ArrayCollection
     *
     * @throws NotFoundHttpException
     */
    public function refreshTokensAction(string $username)
    {
        /** @var User $user */
        $user = $this->userService->getByUsername($username);
        return $user->getRefreshTokens();
    }
}
